<?php 
error_reporting(E_ALL);
mb_internal_encoding("UTF-8");

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$config=require_once 'config.php';
\tpf\utils\Db::initConnection($config['db']);
$curl = new \tpf\utils\Curl();
use Intervention\Image\ImageManager;
$start = time();
$imgEdit = new ImageManager(array('driver' => 'gd'));
$con=tpf\utils\Db::getConnection();
$gen = true;
$index = 1;

// $google = fopen('feed_google.csv', 'w');
// fputcsv($google, array('ID','Item Title','Item description','Final URL','Image URL','Price','Item category'));
// $facebook = fopen('feed_fb.csv', 'w');
// fputcsv($facebook, array('id','availability','condition','description','image_link','link','title', 'brand', 'google_product_category','product_type'));

function download_image1($image_url, $image_file){
    $fp = fopen ($image_file, 'w+');            
    $ch = curl_init($image_url);
    curl_setopt($ch, CURLOPT_FILE, $fp);        
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 1000);      
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
    curl_exec($ch);

    curl_close($ch); 
    fclose($fp);                               
}


do {
	$templates = Texts::find($index);
	$tid = $templates['templateId'];
	$status = $templates['status'];

	if (isset($tid)){
		if ((int)$status === 0) {
		// $templates->status = "1";
		// $templates->save();
		$response = $curl->call("http://service-products.templatemonster.com/api/v1/products/en/".$tid."?_format=json");
		$data = json_decode($response, true);
		$itemTitle = $data['templateTitle'];
		$itemDescription = mb_strimwidth(strip_tags($data['templatePreviewFBTitle'], '<p>'), 0, 500, "...");
		$itemURL = 'https://www.templatemonster.com/moto-cms-3-templates/'.$tid.'.html';
		$imageURL = 'https://s.tmimgcdn.com/scr/'.mb_strimwidth($tid, 0,3).'00/'.$tid.'-big.jpg?build=15';
		$price = $data['price'].' USD'; 
		$itemCategory = $data['templateCategory']['categoryName'];
		$itemBrand = $data['typeShortName'];
		$itemType = $data['templateType']['typeName'];
		$dirname = mb_strimwidth($tid, 0, 3);
		$dir = "img/$dirname";
		$server =  $_SERVER['SERVER_NAME'];
		$imgOutputGoogle = $dir .'/'. $tid .'-google.jpg';
		$imgOutputFacebook = $dir .'/'. $tid .'-fb.jpg';
		$imgG = $server."/".$imgOutputGoogle;
		$imgF = $server."/".$imgOutputFacebook;
		$feeds = Feeds::query()
		->where('prodId', '=', $tid )
		->where('itemTitle', '=', $itemTitle )
		->where('itemDescription', '=', $itemDescription )
		->where('finalURL', '=', $itemURL )
		->where('imageURL', '=', $imageURL )
		->where('price', '=', $price )
		->where('itemCategory', '=', $itemCategory )
		->where('itemBrand', '=', $itemBrand )
		->where('itemType', '=', $itemType )
		->where('imgG', '=', $imgG )
		->where('imgF', '=', $imgF )
		->first();	
		if ($feeds !== null) {
		 continue;
		}
		$feeds = new Feeds();
		$feeds->prodId = $tid;
		$feeds->itemTitle = $itemTitle;
		$feeds->itemDescription = $itemDescription;
		$feeds->finalURL = $itemURL;
		$feeds->imageURL = $imageURL;
		$feeds->price = $price;
		$feeds->itemCategory = $itemCategory;
		$feeds->itemBrand = $itemBrand;
		$feeds->itemType = $itemType;
		$feeds->imgG = $imgG;
		$feeds->imgF = $imgF;
		$feeds->save();


		if (!file_exists($dir)) {
			mkdir($dir);
		} 
		$imgEdit = new ImageManager(array('driver' => 'gd'));
		$imgOutput = $dir .'/'. $tid .'-big.jpg';
		if(!file_exists($imgOutput)){
			download_image1($imageURL, $imgOutput);
		}

		if(!file_exists($imgOutputGoogle)){
			$imgGoogle = $imgEdit->make($imgOutput)->resize(300, null, function($constraint){
				$constraint->aspectRatio();
			})->crop(300,300, 0)->save($imgOutputGoogle);
		}
		if(!file_exists($imgOutputFacebook)){
			$imgGoogle = $imgEdit->make($imgOutput)->resize(1200,null, function($constraint){
				$constraint->aspectRatio();
			})->crop(1200, 630, 0)->save($imgOutputFacebook);
		}


		$google = fopen('feed_google.csv', 'a');
		fputcsv($google, array($tid, $itemTitle, $itemDescription, $itemURL, $imgG, $price, $itemCategory));
		$facebook = fopen('feed_fb.csv', 'a');
		fputcsv($facebook, array($tid, "in stock", "new", $itemDescription, $imgF, $itemURL, $itemTitle, $price, $itemBrand, $itemCategory, $itemType));
		}

	} else {
		$gen = false;
	}

	++$index;


} while($gen);








// $templates = Texts::chunk(10, function($template){
// 	$curl = new \tpf\utils\Curl();
// 	foreach ($template as $item) {
// 		$tid = $item->templateId;
// 		$status = $item->status;
// 		if ((int)$status === 0) {
// 		$item->status = "1";
// 		$item->save();
// 	$response = $curl->call("http://service-products.templatemonster.com/api/v1/products/en/".$tid."?_format=json");
// 		$data = json_decode($response, true);
// 		$itemTitle = $data['templateTitle'];
// 		$itemDescription = mb_strimwidth(strip_tags($data['templatePreviewFBTitle'], '<p>'), 0, 500, "...");
// 		$itemURL = 'https://www.templatemonster.com/moto-cms-3-templates/'.$tid.'.html';
// 		$imageURL = 'https://s.tmimgcdn.com/scr/'.mb_strimwidth($tid, 0,3).'00/'.$tid.'-big.jpg?build=15';
// 		$price = $data['price'].' USD'; 
// 		$itemCategory = $data['templateCategory']['categoryName'];
// 		$itemBrand = $data['typeShortName'];
// 		$itemType = $data['templateType']['typeName'];
// 		$dirname = mb_strimwidth($tid, 0, 3);
// 		$dir = "img/$dirname";
// 		$server =  $_SERVER['SERVER_NAME'];
// 		$imgOutputGoogle = $dir .'/'. $tid .'-google.jpg';
// 		$imgOutputFacebook = $dir .'/'. $tid .'-fb.jpg';
// 		$imgG = $server."/".$imgOutputGoogle;
// 		$imgF = $server."/".$imgOutputFacebook;
// 		$feeds = Feeds::query()
// 		->where('prodId', '=', $tid )
// 		->where('itemTitle', '=', $itemTitle )
// 		->where('itemDescription', '=', $itemDescription )
// 		->where('finalURL', '=', $itemURL )
// 		->where('imageURL', '=', $imageURL )
// 		->where('price', '=', $price )
// 		->where('itemCategory', '=', $itemCategory )
// 		->where('itemBrand', '=', $itemBrand )
// 		->where('itemType', '=', $itemType )
// 		->where('imgG', '=', $imgG )
// 		->where('imgF', '=', $imgF )
// 		->first();	
// 		if ($feeds !== null) {
// 		 continue;
// 		}
// 		$feeds = new Feeds();
// 		$feeds->prodId = $tid;
// 		$feeds->itemTitle = $itemTitle;
// 		$feeds->itemDescription = $itemDescription;
// 		$feeds->finalURL = $itemURL;
// 		$feeds->imageURL = $imageURL;
// 		$feeds->price = $price;
// 		$feeds->itemCategory = $itemCategory;
// 		$feeds->itemBrand = $itemBrand;
// 		$feeds->itemType = $itemType;
// 		$feeds->imgG = $imgG;
// 		$feeds->imgF = $imgF;
// 		$feeds->save();

// 			// $dirname = mb_strimwidth($tid, 0, 3);
// 			// $dir = "img/$dirname";
// 			// if (!file_exists($dir)) {
// 			// 	mkdir($dir);
// 			// } 
// 			// $imgEdit = new ImageManager(array('driver' => 'gd'));
// 			// $imgOutput = $dir .'/'. $tid .'-big.jpg';
// 			// $imgOutputGoogle = $dir .'/'. $tid .'-google.jpg';
// 			// $imgOutputFacebook = $dir .'/'. $tid .'-fb.jpg';
// 			// $imgG = $server."/".$imgOutputGoogle;
// 			// $imgF = $server."/".$imgOutputFacebook;

// 			// if(!file_exists($imgOutput)){
// 			// 	download_image1($imageURL, $imgOutput);
// 			// }

// 			// if(!file_exists($imgOutputGoogle)){
// 			// 	$imgGoogle = $imgEdit->make($imgOutput)->resize(300, null, function($constraint){
// 			// 		$constraint->aspectRatio();
// 			// 	})->crop(300,300, 0)->save($imgOutputGoogle);
// 			// }
// 			// if(!file_exists($imgOutputFacebook)){
// 			// 	$imgGoogle = $imgEdit->make($imgOutput)->resize(1200,null, function($constraint){
// 			// 		$constraint->aspectRatio();
// 			// 	})->crop(1200, 630, 0)->save($imgOutputFacebook);
// 			// }
		
			




// 			$google = fopen('feed_google.csv', 'a');
// 			fputcsv($google, array($tid, $itemTitle, $itemDescription, $itemURL, $imgG, $price, $itemCategory));
// 			$facebook = fopen('feed_fb.csv', 'a');
// 			fputcsv($facebook, array($tid, "in stock", "new", $itemDescription, $imgF, $itemURL, $itemTitle, $price, $itemBrand, $itemCategory, $itemType));

// 		}

		
// 	}

// });













// $feed = Feeds::chunk(10, function($feeds){

// 	foreach ($feeds as $i) {
// 			$tid = $i->prodid;
// 			$itemTitle = $i->itemTitle;
// 			$itemDescription = $i->itemDescription;
// 			$itemURL = $i->finalURL;
// 			$imageURL = $i->imageURL;
// 			$price = $i->price;
// 			$itemCategory = $i->itemCategory;

// 			$google = fopen('feed_google.csv', 'a');
// 			fputcsv($file, array($tid, $itemTitle, $itemDescription, $itemURL, $imageURL, $price, $itemCategory));
// 			$facebook = fopen('feed_google.csv', 'a');
// 			fputcsv($file, array($tid, "in stock", "new", $itemDescription, $imageURL, $itemURL, $itemTitle, $price, $itemBrand, $itemCategory, $itemType));

// 			$dirname = mb_strimwidth($tid, 0, 3);
// 			$dir = "img/$dirname";
// 			if (!file_exists($dir)) {
// 				mkdir($dir);
// 			} 
// 			$imgEdit = new ImageManager(array('driver' => 'gd'));
// 			$imgOutput = $dir .'/'. $tid .'-big.jpg';
// 			$imgOutputGoogle = $dir .'/'. $tid .'-google.jpg';
// 			$imgOutputFacebook = $dir .'/'. $tid .'-fb.jpg';

// 			if(!file_exists($imgOutput)){
// 				download_image1($imageURL, $imgOutput);
// 			}

// 			if(!file_exists($imgOutputGoogle)){
// 				$imgGoogle = $imgEdit->make($imgOutput)->resize(300, null, function($constraint){
// 					$constraint->aspectRatio();
// 				})->crop(300,300, 0)->save($imgOutputGoogle);
// 			}
// 			if(!file_exists($imgOutputFacebook)){
// 				$imgGoogle = $imgEdit->make($imgOutput)->resize(1200,null, function($constraint){
// 					$constraint->aspectRatio();
// 				})->crop(1200, 630, 0)->save($imgOutputFacebook);
// 			}
		
// 	}
	

// });



