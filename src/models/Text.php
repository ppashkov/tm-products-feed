<?php
use \Illuminate\Database\Eloquent\Model;
class Texts extends Model
{
	protected $table = 'tm_pr_feed';
	protected $primaryKey = 'id';
	public $timestamps = false;
	protected $fillable = [
		'templateId'
	];
}

class Feeds extends Model
{
	protected $table = 'feed';
	protected $primaryKey = 'id';
	public $timestamps = false;
	protected $fillable = [
		'prodid', 
		'itemTitle',
		'itemDescription',
		'finalURL',
		'imageURL',
		'price',
		'itemCategory',
	];
}

class Feeds2 extends Model
{
	protected $table = 'feed2';
	protected $primaryKey = 'id';
	public $timestamps = false;
	protected $fillable = [
		'prodid', 
		'itemTitle',
		'itemDescription',
		'finalURL',
		'imageURL',
		'price',
		'itemCategory',
	];
}
